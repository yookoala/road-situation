import os
import time
from datetime import datetime
from flask import Flask, request, Response

app = Flask(__name__)


def cycle(generator):
    """Loop calling the generator of generator, and yield from it"""
    while True:
        yield from generator()


def gen_generator(callback, *args):
    """Create a generator of generator callable."""
    def ret():
        yield from callback(*args)
    return ret


def files_in(target_dir):
    """Generator of files inside a directory, repeating."""
    for filepath, _, filenames in os.walk(target_dir):
        for file in filenames:
            if file.endswith('.jpg'):
                yield os.path.join(filepath, file)


def tick(seconds: int, refresh: float = 0.1):
    """Returns a ticker to limit the rate of inner generator
    to only return according to refresh time (second)
    """
    def ticker(generator):
        start = datetime.now().timestamp()
        for item in generator:
            end = datetime.now().timestamp()
            while end - start < seconds:
                yield item
                time.sleep(refresh)
                end = datetime.now().timestamp()
            start = end
    return ticker


def files_to_frames(filenames):
    """From a stream of filenames, yield a stream of image frame
    """
    for filename in filenames:
        file = open(filename, "rb")
        data = file.read()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + data + b'\r\n\r\n')
        file.close()


def compose(*callables):
    """Compose multiple single argument functions
    into a single function.
    """
    def composed(arg):
        for func in reversed(callables):
            arg = func(arg)
        return arg
    return composed


@app.route('/video_feed')
def video_feed():
    """A flask endpoint to display endless stream of images
    """
    spot_id = request.args.get('id', '')
    per_slide = int(request.args.get('per_slide', '2'))
    stream_contents_endlessly_from = compose(
        files_to_frames,
        tick(per_slide),
        cycle,
    )
    filenames = gen_generator(files_in, "./data/tmp/{}".format(spot_id))
    return Response(stream_contents_endlessly_from(filenames),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
