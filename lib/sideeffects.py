"""Selenium related helper functions"""

import json
import os
from time import sleep
from typing import Callable, Type
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile


def play_video(driver: Type[WebDriver], target: str, main_window: int):
    """
    Controls a given webdriver, browsing the road situation page,
    to locate and play the video streaming element.
    """
    driver.switch_to.window(main_window)

    WebDriverWait(driver, 10).until(FunctionExists("weblive"))
    driver.execute_script("weblive({});".format(json.dumps(target)))

    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "videolink_html5_api")))
    sleep(0.5)
    WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.CLASS_NAME, "vjs-big-play-button")))
    driver.find_element_by_class_name("vjs-big-play-button").click()


def get_driver_instance(base_path: str, driver_name: str,
                        **kwargs) -> Type[WebDriver]:
    """Returns a driver instance"""
    profile = FirefoxProfile(os.path.join(
        base_path, "data", "firefox-profile"))
    driver_path = os.path.join(base_path, "bin", driver_name)
    return webdriver.Firefox(
        executable_path=driver_path,
        firefox_profile=profile,
        **kwargs,
    )


def close_all_window_except(window_handle):
    def close_all(driver_instance):
        for handle in driver_instance.window_handles:
            if handle != window_handle:
                driver_instance.switch_to.window(handle)
                driver_instance.close()
        driver_instance.switch_to.window(window_handle)
    return close_all


def initialize_window(
        driver_instance: Type[WebDriver],
        url: str,
        init_script: str = "golive()",
        width: int = 800,
        height: int = 600,
) -> (int, Callable):
    """Initialize a driver_instance to open main window that connects to URL"""
    driver_instance.get(url)
    driver_instance.set_window_size(width, height)
    main_window = driver_instance.current_window_handle
    driver_instance.execute_script(init_script)
    sleep(1)

    def reload(driver_instance: Type[WebDriver]):
        for handle in driver_instance.window_handles:
            driver_instance.switch_to.window(handle)
            if driver_instance.current_window_handle != main_window:
                driver_instance.close()
        driver_instance.switch_to.window(main_window)
        driver_instance.get(url)
        driver_instance.execute_script(init_script)
        sleep(1)

    return (
        main_window,
        reload,
    )


class FunctionExists(object):
    """ Check if the function exists
    in global namespace.
    """
    def __init__(self, func_name: str):
        self.func_name = func_name

    def __call__(self, driver):
        try:
            result = driver.execute_script(
                "return typeof {}".format(self.func_name))
            if result == "function":
                return True
            return False
        except EC.StaleElementReferenceException:
            return False


class VideoPresencesAndPlaying(object):
    """ Check if the located video element is playing
    """
    def __init__(self, locator):
        self.locator = locator

    def __call__(self, driver):
        try:
            element = driver.find_element(*self.locator)
            if element.is_displayed() is False:
                return False
            if (element.get_attribute('paused') == "true"
                    or element.get_attribute('paused') is True):
                return False
            if (element.get_attribute('ended') == "true"
                    or element.get_attribute('ended') is True):
                return False
            return True
        except EC.StaleElementReferenceException:
            return False


class ImagePresencesAndLoaded:
    """ Check if the located image element has completed download
    """
    def __init__(self, locator):
        self.locator = locator

    def __call__(self, driver):
        try:
            element = driver.find_element(*self.locator)
            if element.is_displayed() is False:
                return False
            if (element.get_attribute('complete') != "true"
                    and element.get_attribute('complete') is not True):
                return False
            if element.get_attribute('naturalWidth') == "0":
                return False
            return True
        except EC.StaleElementReferenceException:
            return False


class HasWindowMoreThan:
    """Check if the driver instance is handling
    multiple windows.
    """
    def __init__(self, num):
        self.num = num

    def __call__(self, driver):
        return (len(driver.window_handles) > self.num)


class DoSideTaskIf:
    """Define a sidetask to be perform if
    the method check returns True. This method
    return True no matter what.
    """
    def __init__(self, method, side_task):
        self.method = method
        self.side_task = side_task

    def __call__(self, driver):
        if self.method(driver):
            self.side_task(driver)
        return True


class Or:
    """Or of 2 methods"""

    def __init__(self, *methods):
        self.methods = methods

    def __call__(self, driver):
        for method in self.methods:
            if method(driver):
                return True
        return False


class And:
    """Or of 2 methods"""

    def __init__(self, *methods):
        self.methods = methods

    def __call__(self, driver):
        for method in self.methods:
            if not method(driver):
                return False
        return True
