"""Parsing options and environment variables"""

from getopt import getopt
from typing import Sequence
import sys


def read_opts(
        args: Sequence[str],
        driver_name: str = "geckodriver",
) -> (str, str, Sequence[str]):
    """Read options from system arguments"""
    if len(args) < 1:
        print("action is not specified")
        sys.exit(1)
    action = args[0]

    if action not in ["live", "snapshot"]:
        print("{} is not a valid action".format(action))

    targets = []
    opts, _ = getopt(args[1:], "hd:t:", [
        "help", "driver=", "target=",
    ])
    for key, value in opts:
        if key in ("-d", "--driver"):
            driver_name = value
        elif key in ("-t", "--target"):
            if action == "live" and value not in ["hk", "kl", "tw", "tm"]:
                print("{} is not a valid target name.".format(value))
                print("valid options: hk, kl, tw, tm")
                sys.exit(1)
            targets.append(value)
    return (driver_name, action, targets)
