# Realtime Road Situation

A simple program to keep a browser running to show the
Hong Kong Transport Department (運輸署) realtime road
situation streaming.

## License

This software is distributed under the MIT License. A
copy of the license is available [here](LICENSE.md).
