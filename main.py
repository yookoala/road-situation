#!/usr/bin/env python3

"""A program to keep a window open to official HK road situation broadcast"""

import getopt
import json
import logging
import os
import re
import sys
from datetime import datetime
from time import sleep
from typing import Sequence, Type, Union
from multiprocessing import Process
from urllib.request import urlretrieve
from selenium.common.exceptions import (
    TimeoutException, NoSuchWindowException, WebDriverException,
)
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait
from lib.env import read_opts
from lib.sideeffects import (
    play_video,
    get_driver_instance,
    initialize_window,
    close_all_window_except,
    And,
    DoSideTaskIf,
    HasWindowMoreThan,
    ImagePresencesAndLoaded,
    VideoPresencesAndPlaying,
)


def run_live(driver_name: str, target: str):
    """Main program body for maintianing a live broadcast"""

    # declare main logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(
        logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    logger.addHandler(handler)

    # initialization stage
    logger.debug("driver=%s, target=%s", driver_name, target)
    driver_instance = get_driver_instance(
        base_path=os.path.abspath(os.path.dirname(__file__)),
        driver_name=driver_name,
    )
    main_window, reload = initialize_window(
        driver_instance, "http://traffic.td.gov.hk/SwitchCenter.do")

    # CSS to override default style and
    # make the video element covering up the whole window.
    css = [
        """
        body {
            overflow: hidden;
        }
        """,
        """
        #videolink_html5_api {
            position: fixed;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
        }
        """,
        """
        .vjs-control-bar {
            visibility: hidden !important;
        }
        """,
    ]

    # main loop to monitor video playing status
    while True:
        try:
            logger.debug("locate and play the video")
            play_video(driver_instance, target, main_window)
            driver_instance.execute_script(
                "document.title = 'Road Situation: {}'".format(target))
            for rule in css:
                driver_instance.execute_script(
                    "document.styleSheets[0].insertRule({}, 0)".format(
                        json.dumps(rule)))
            WebDriverWait(driver_instance, 3600, 0.5).until_not(
                And(
                    DoSideTaskIf(
                        HasWindowMoreThan(1),
                        close_all_window_except(main_window),
                    ),
                    VideoPresencesAndPlaying((By.ID, "videolink_html5_api")),
                ),
            )

        except NoSuchWindowException:
            logger.error("Main window has been closed unexpectedly. abort")
            sys.exit(1)
        except TimeoutException as error:
            logger.error(error)
            reload(driver_instance)
        except WebDriverException as error:
            logger.error(error)
            reload(driver_instance)
        except KeyboardInterrupt:
            logger.info("user quit. exit now.")
            if os.name == "nt":
                logger.debug("closing all controlling windows")
                for handle in driver_instance.window_handles:
                    driver_instance.switch_to.window(handle)
                    driver_instance.close()
                logger.debug("closing all controlling windows -> done")
            break


def gen_spot_ids(driver_instance: Type[WebDriver]):
    """Find all spot_id, and trigger download of all snapshots"""
    regex = re.compile(r"^javascript:viewsnapshot\('(\d+)'\);$")
    spots = driver_instance.find_elements_by_xpath("//area[@shape='circle']")
    for spot in spots:
        if regex.match(spot.get_attribute("href")):
            yield regex.sub("\\1", spot.get_attribute("href"))


def get_snapshot(
        driver_name: str,
        targets: Union[Sequence[str], bool] = False,
):
    """Get snapshot of all targets specified. If no target specified,
    will try to discover and get all of them.
    """
    # declare main logger
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(
        logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    logger.addHandler(handler)

    # initialization stage
    logger.debug("driver=%s", driver_name)
    options = FirefoxOptions()
    options.add_argument("-headless")
    driver_instance = get_driver_instance(
        base_path=os.path.abspath(os.path.dirname(__file__)),
        driver_name=driver_name,
        firefox_options=options,
    )
    _, _ = initialize_window(
        driver_instance,
        url="http://traffic.td.gov.hk/ClickProcessor.do?area_id=hk",
        init_script="",
    )
    now = datetime.now().strftime('%Y%m%d%H%M%S')

    # determine the targetted spot_ids,
    # if not specified, parse all spot_ids from the page.
    if targets is False:
        spot_ids = gen_spot_ids(driver_instance)
    else:
        spot_ids = targets

    # download images
    for spot_id in spot_ids:
        driver_instance.execute_script("viewsnapshot({})".format(
            json.dumps(spot_id)))
        sleep(0.1)
        WebDriverWait(driver_instance, 20, 0.05).until(
            ImagePresencesAndLoaded((By.XPATH, "//img[@name='snapshot']")))
        src = driver_instance.find_element_by_xpath(
            "//img[@name='snapshot']").get_attribute("src")
        os.makedirs(os.path.join("data", "tmp", spot_id), exist_ok=True)
        urlretrieve(src, os.path.join("data", "tmp", spot_id, '{}.jpg').format(
            now))

    driver_instance.close()


if __name__ == '__main__':

    try:
        # get option values
        DRIVER_NAME, ACTION, TARGETS = read_opts(sys.argv[1:])
    except getopt.GetoptError as error:
        print(error)
        sys.exit(1)

    if ACTION == "live":
        for TARGET in TARGETS:
            Process(target=run_live, args=(DRIVER_NAME, TARGET)).start()
    elif ACTION == "snapshot":
        get_snapshot(DRIVER_NAME, TARGETS)
